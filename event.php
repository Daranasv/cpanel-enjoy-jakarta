<?php
include_once('include/connect.php');
if(isset($_POST['kirim'])){
  $nama_event = $_POST['nama_event'];
  $tanggal_event = $_POST['tanggal_event'];
  $kontent = $_POST['kontent'];

  $img_src=$_FILES['img_src']['name'];

  $uploaddir='photo/';
  $alamatfile=$uploaddir.$img_src;

  //periksa jika proses upload berjalan sukses
  $upload=move_uploaded_file($_FILES['img_src']['tmp_name'],$alamatfile);

  $query =mysql_query("INSERT INTO `event`(`nama_event`, `tanggal_event`, `kontent`, `img_src`) VALUES ('$nama_event','$tanggal_event','$kontent','$img_src')");
  if ($query) {
    echo "<div class='alert alert-success'>
              <button type='button' class='close' data-dismiss='alert'>×</button>
              <strong>Well done!</strong> You successfully input this data.
            </div>";
  } else {
    echo "<div class='alert alert-error'>
              <button type='button' class='close' data-dismiss='alert'>×</button>
              <strong>Oh snap!!</strong> Change a few things up and try submitting again.
            </div>";
  }
}
?>
<?php 
	if(isset($_GET['action']) && $_GET['action']=='edit'){
		$id 				= $_GET['id'];
		$resedit 			= mysql_query('SELECT * FROM event where id='.$id);
		$rest 				= mysql_fetch_array($resedit);
		$nama_event 		= $rest['nama_event'];
		$tanggal_event		= $rest['tanggal_event'];
		$kontent			= $rest['kontent'];
		$url 				= "edit_event.php";
	}else{
		$id 				= "";
		$nama_event 		= "";
		$tanggal_event		= "";
		$kontent			= "";
		$url 				= "";
	}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"
   />
   <link rel="stylesheet" type="text/css" href="aset/css/bootstrap.css">
   <link rel="stylesheet" type="text/css" href="aset/css/style.css" /> 
  <!--[if gt IE 8]>
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />        
  <![endif]--> 
    <title>Enjoy Jakarta</title>
    <style>
    .wrappers {
      width: 300px;
      margin: 50px 50px 50px 0px;
    }
    </style>
  </head>
  <body>
  <div class="container">
    <div class="wrappers">
    <div class="wellss">
    <form action="<?php echo $url;?>" method="POST" enctype="multipart/form-data">
    <legend>Event</legend>
      <label>Nama Event</label>
      <div class="input-prepend"><span class="add-on"><i class="icon-map-marker"></i></span>
      <input type="text" id="" name="nama_event" value="<?php echo $nama_event?>">
      <input type="hidden" id="" name="id_event" value="<?php echo $id?>">
      </div>

      <label>Tanggal Event</label>
      <div class="input-prepend"><span class="add-on"><i class="icon-calendar"></i></span>
      <input type="date" id="" name="tanggal_event" value="<?php echo $tanggal_event?>">
      </div>

      <label>Konten</label>
      <textarea name="kontent" rows="6" style="min-width:235px;max-width:235px;">
      	<?php echo $kontent?>
      </textarea>

      <label>Photo</label>
      <div class="input-prepend"><input type="file" id="" name="img_src"></div>

      <a href='event_list.php' class='btn btn-danger'>Kembali</a>
      <input type="submit" class="btn btn-success" value="Submit" name="kirim" />
      </form>
  </div>
</div>
    </div>
  </body>
</html>