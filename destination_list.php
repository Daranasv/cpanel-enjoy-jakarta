<?php include('include/connect.php');?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
	<title>Enjoy Jakarta</title>

	<!--<link rel="stylesheet" type="text/css" href="aset/css/bootstrap.css">-->
   	<link rel="stylesheet" type="text/css" href="aset/css/style.css" />
   	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
	<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script> 

   	<style>
    .wrappers, .alert {
      margin:70px auto;
    }
    </style> 
</head>
<body>
	<div class='container'>
		<div class='row'>
			<div class='col-md-6 text-left'>
				<a href='menu.php' class='btn btn-default'>Kembali</a>
			</div>
			<div class='col-xs-6 text-right'>
				<a href='destination.php' class='btn btn-primary'>Tambah</a>
			</div>
		</div>
		<div class='row'>
			<div class="col-md-12 text-center">
			<?php 
			if(isset($_SESSION['alert'])){
				echo $_SESSION['alert'];
				//unset($_SESSION['alert']);
			}
			?>
				<h4>List Destination</h4>
			</div>
		</div>
		<div class='row'>
			<div class='col-md-12'>
				<form class='form-inline' role='form'>
					<div class='form-group'>
						<label class="sr-only" for="exampleInputEmail2">Tipe</label>
    					<select class="form-control" id='select_tipe'>
    					<option value=''>-Pilih Tipe-</option>
    					<?php 
    						$q = mysql_query('SELECT DISTINCT tipe FROM  `destination`');
   							while($data = mysql_fetch_array($q)):	
    					?>
						  <option value='<?php echo $data["tipe"] ?>'><?php echo $data["tipe"] ?></option>
						<?php endwhile; ?>
						</select>
    					<button type='button' class='btn btn-primary' id='btn-cari'>Search</button>
					</div>

				</form>
			</div>
		</div>
		<div class='row'>
			<div class='col-md-12'>
				<table class='table table-hover col-md-12'>
					<thead>
						<tr>
							<th>ID</th>
							<th>Nama</th>
							<th>Alamat</th>
							<th>Telepon</th>
							<th>Latitude</th>
							<th>Longitude</th>
							<!-- <th>Deskripsi</th> -->
							<th>Tipe</th>
							<th style="max-width:180px">Email</th>
							<th style="word-wrap: break-word;width:100px">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php 
							if(!isset($_GET['tipe']) || $_GET['tipe']==''){
								$res = mysql_query('select * from destination');	
							}
							else{
								$res = mysql_query('select * from destination where tipe like "%'.$_GET['tipe'].'%"');	
							}
							  while($data_table = mysql_fetch_array($res)):
						?>
						<tr>
							<td><?php echo $data_table['id']?></td>
							<td><?php echo  $data_table['nama']?></td>
							<td><?php echo  $data_table['alamat']?></td>
							<td><?php echo $data_table['telepon']?></td>
							<td><?php echo  $data_table['lat']?></td>
							<td><?php echo $data_table['long']?></td>
							<!-- <td><?php echo $data_table['desk']?></td> -->
							<td><?php echo $data_table['tipe']?></td>
							<td style="word-wrap: break-word;max-width:150px"><?php echo $data_table['email']?></td>
							<td style="width:100px">
								<a href='destination.php?action=edit&id=<?php echo $data_table["id"]?>' class='btn btn-warning btn-xs'><i class="icon-edit icon-white"></i>Edit</a>
								<a href='#' onclick='del(<?php echo $data_table['id']?>)' class='btn btn-danger btn-xs'><i class="icon-trash icon-white"></i>Delete</a>
							</td>
						</tr>
					<?php endwhile;?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<script>
		function del(id){
			if(confirm('Anda Yakin Akan Menghapus Data?')){
				window.location="delete_destination.php?id="+id;
			}else{
				return false;
			}
		}
		$(document).ready(function(){
			//alert('aaaa');
			$('#btn-cari').click(function(){
				var tipe = $('#select_tipe').val();
				window.location='destination_list.php?tipe='+tipe;
			});
		});
	</script>
</body>
</html>