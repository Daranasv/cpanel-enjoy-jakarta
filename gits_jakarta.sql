-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 20, 2013 at 07:44 PM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gits_jakarta`
--

-- --------------------------------------------------------

--
-- Table structure for table `destination`
--

CREATE TABLE IF NOT EXISTS `destination` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `telepon` varchar(25) NOT NULL,
  `url_photo` varchar(150) NOT NULL,
  `lat` varchar(25) NOT NULL,
  `long` varchar(25) NOT NULL,
  `desk` varchar(500) NOT NULL,
  `tipe` enum('hotel','golf','shopping','kuliner','hiburan','museum','pusat rekeasi','spa','mice') NOT NULL,
  `email` varchar(50) NOT NULL,
  `link` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `destination`
--

INSERT INTO `destination` (`id`, `nama`, `alamat`, `telepon`, `url_photo`, `lat`, `long`, `desk`, `tipe`, `email`, `link`) VALUES
(1, 'nama 1', 'alama 1', '0283930', '/url/photo', '222', '2323', 'deskripsi asala 1', 'hotel', '', ''),
(2, 'nama 2', 'alamat 2', '0322222', 'url/photo/2', '23333', '4343', 'ini deskripsi asal', 'hotel', '', ''),
(3, 'nama', 'lasdjasd', '982374823', 'jasdksad.jpg', '21312', '123324', 'sfsdfs', 'hotel', '', ''),
(4, 'nama1', 'lasdjasd1', '9823748123', 'jasd1ksad.jpg', '213112', '1213324', 'sfs1dfs', 'hotel', '', ''),
(5, 'Darana Sukma Vidya', 'alamat', 'telepon', 'korea.png', 'latitude', 'longtitude', 'deskripsi', 'hotel', '', ''),
(6, 'Windhy Vibe', 'alamat', 'telepon', 'thailand.png', 'latitude', 'longtitude', 'deskripsi', 'hotel', '', ''),
(7, 'rer', 'fdfds', 'fdsfds', 'taiwan.png', 'rewr', 'rewr', 'dsvds', 'hotel', '', ''),
(8, 'rer', 'fdfds', 'fdsfds', 'taiwan.png', 'rewr', 'rewr', 'dsvds', 'hotel', '', ''),
(9, 'rer', 'fdfds', 'fdsfds', 'taiwan.png', 'rewr', 'rewr', 'dsvds', 'hotel', '', ''),
(10, 'nama', 'alamat', 'telepon', '', 'latitude', 'longtitude', 'email', 'golf', '', ''),
(11, 'nama', 'alamat', 'telepon', '', 'latitude', 'longtitude', '', 'golf', 'email', 'link'),
(12, 'test', 'test', 'test', '', 'test', 'test', '', 'spa', 'test', 'test'),
(13, 'test2', 'test2', 'test2', '', 'test2', 'test2', '', '', 'test2', 'test2'),
(14, 'test3', 'test3', 'test3', '', 'test3', 'test3', '', 'pusat rekeasi', 'test3', 'test3');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_event` varchar(100) NOT NULL,
  `tanggal_event` date NOT NULL,
  `kontent` longtext NOT NULL,
  `img_src` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `nama_event`, `tanggal_event`, `kontent`, `img_src`) VALUES
(1, 'Event 1', '2013-11-25', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla facilisis magna massa, quis commodo odio mollis commodo. Proin semper sit amet tellus ut porta. Donec sed semper tortor. Nullam eget tortor purus. Etiam ac neque pharetra, ultrices arcu a, tincidunt libero. Pellentesque consequat cursus venenatis. Aenean iaculis metus velit, vel tincidunt mauris laoreet ornare. Nulla convallis arcu a leo dapibus suscipit. Quisque sed tristique odio, vitae sollicitudin velit. Morbi sit amet purus non odio aliquam consequat. Nullam accumsan ultricies tortor, ac eleifend purus venenatis ac. Mauris leo enim, hendrerit ac lorem ut, iaculis viverra purus. Nullam pretium blandit elit, at pulvinar diam sollicitudin a. Vestibulum eu elementum dui. Maecenas accumsan accumsan interdum.', ''),
(2, 'event 2', '2013-11-12', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla facilisis magna massa, quis commodo odio mollis commodo. Proin semper sit amet tellus ut porta. Donec sed semper tortor. Nullam eget tortor purus. Etiam ac neque pharetra, ultrices arcu a, tincidunt libero. Pellentesque consequat cursus venenatis. Aenean iaculis metus velit, vel tincidunt mauris laoreet ornare. Nulla convallis arcu a leo dapibus suscipit. Quisque sed tristique odio, vitae sollicitudin velit. Morbi sit amet purus non odio aliquam consequat. Nullam accumsan ultricies tortor, ac eleifend purus venenatis ac. Mauris leo enim, hendrerit ac lorem ut, iaculis viverra purus. Nullam pretium blandit elit, at pulvinar diam sollicitudin a. Vestibulum eu elementum dui. Maecenas accumsan accumsan interdum.', ''),
(3, 'event 3', '2013-12-10', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla facilisis magna massa, quis commodo odio mollis commodo. Proin semper sit amet tellus ut porta. Donec sed semper tortor. Nullam eget tortor purus. Etiam ac neque pharetra, ultrices arcu a, tincidunt libero. Pellentesque consequat cursus venenatis. Aenean iaculis metus velit, vel tincidunt mauris laoreet ornare. Nulla convallis arcu a leo dapibus suscipit. Quisque sed tristique odio, vitae sollicitudin velit. Morbi sit amet purus non odio aliquam consequat. Nullam accumsan ultricies tortor, ac eleifend purus venenatis ac. Mauris leo enim, hendrerit ac lorem ut, iaculis viverra purus. Nullam pretium blandit elit, at pulvinar diam sollicitudin a. Vestibulum eu elementum dui. Maecenas accumsan accumsan interdum.', ''),
(4, 'event 4', '2013-12-19', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla facilisis magna massa, quis commodo odio mollis commodo. Proin semper sit amet tellus ut porta. Donec sed semper tortor. Nullam eget tortor purus. Etiam ac neque pharetra, ultrices arcu a, tincidunt libero. Pellentesque consequat cursus venenatis. Aenean iaculis metus velit, vel tincidunt mauris laoreet ornare. Nulla convallis arcu a leo dapibus suscipit. Quisque sed tristique odio, vitae sollicitudin velit. Morbi sit amet purus non odio aliquam consequat. Nullam accumsan ultricies tortor, ac eleifend purus venenatis ac. Mauris leo enim, hendrerit ac lorem ut, iaculis viverra purus. Nullam pretium blandit elit, at pulvinar diam sollicitudin a. Vestibulum eu elementum dui. Maecenas accumsan accumsan interdum.', ''),
(5, 'event 5', '2013-11-21', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla facilisis magna massa, quis commodo odio mollis commodo. Proin semper sit amet tellus ut porta. Donec sed semper tortor. Nullam eget tortor purus. Etiam ac neque pharetra, ultrices arcu a, tincidunt libero. Pellentesque consequat cursus venenatis. Aenean iaculis metus velit, vel tincidunt mauris laoreet ornare. Nulla convallis arcu a leo dapibus suscipit. Quisque sed tristique odio, vitae sollicitudin velit. Morbi sit amet purus non odio aliquam consequat. Nullam accumsan ultricies tortor, ac eleifend purus venenatis ac. Mauris leo enim, hendrerit ac lorem ut, iaculis viverra purus. Nullam pretium blandit elit, at pulvinar diam sollicitudin a. Vestibulum eu elementum dui. Maecenas accumsan accumsan interdum.', ''),
(6, 'NgandroidFest', '0000-00-00', 'ini adalah tese', 'india.png');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) NOT NULL,
  `tanggal` varchar(100) NOT NULL,
  `konten` varchar(5000) NOT NULL,
  `img_src` varchar(150) NOT NULL,
  `hit` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `judul`, `tanggal`, `konten`, `img_src`, `hit`) VALUES
(2, 'Berita 1', '2013-11-26', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ac vulputate odio. Donec fermentum augue laoreet mi mattis viverra. Morbi ac molestie tortor. Pellentesque non nisi vitae purus cursus tempor. Duis ipsum risus, hendrerit nec nulla eget, scelerisque convallis lacus. Donec quis commodo nibh, sit amet sollicitudin nisl. Interdum et malesuada fames ac ante ipsum primis in faucibus. Proin lobortis auctor diam, eget scelerisque diam ullamcorper non. Aenean et diam non lectus congue tempor. Sed ut fermentum ligula, non molestie nibh. Etiam vehicula libero in dolor egestas, in lacinia nisi placerat. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris auctor imperdiet nibh vel mollis. Ut sed libero aliquet, volutpat turpis et, aliquam est. Curabitur condimentum nunc ligula, id fringilla turpis facilisis eu. Nam cursus sit amet diam sit amet convallis.', '', 0),
(3, 'Berita 2', '2013-11-27', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ac vulputate odio. Donec fermentum augue laoreet mi mattis viverra. Morbi ac molestie tortor. Pellentesque non nisi vitae purus cursus tempor. Duis ipsum risus, hendrerit nec nulla eget, scelerisque convallis lacus. Donec quis commodo nibh, sit amet sollicitudin nisl. Interdum et malesuada fames ac ante ipsum primis in faucibus. Proin lobortis auctor diam, eget scelerisque diam ullamcorper non. Aenean et diam non lectus congue tempor. Sed ut fermentum ligula, non molestie nibh. Etiam vehicula libero in dolor egestas, in lacinia nisi placerat. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris auctor imperdiet nibh vel mollis. Ut sed libero aliquet, volutpat turpis et, aliquam est. Curabitur condimentum nunc ligula, id fringilla turpis facilisis eu. Nam cursus sit amet diam sit amet convallis.', '', 0),
(4, 'Berita 3', '2013-11-28', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ac vulputate odio. Donec fermentum augue laoreet mi mattis viverra. Morbi ac molestie tortor. Pellentesque non nisi vitae purus cursus tempor. Duis ipsum risus, hendrerit nec nulla eget, scelerisque convallis lacus. Donec quis commodo nibh, sit amet sollicitudin nisl. Interdum et malesuada fames ac ante ipsum primis in faucibus. Proin lobortis auctor diam, eget scelerisque diam ullamcorper non. Aenean et diam non lectus congue tempor. Sed ut fermentum ligula, non molestie nibh. Etiam vehicula libero in dolor egestas, in lacinia nisi placerat. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris auctor imperdiet nibh vel mollis. Ut sed libero aliquet, volutpat turpis et, aliquam est. Curabitur condimentum nunc ligula, id fringilla turpis facilisis eu. Nam cursus sit amet diam sit amet convallis.', '', 0),
(5, 'Berita 3', '2013-11-28', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ac vulputate odio. Donec fermentum augue laoreet mi mattis viverra. Morbi ac molestie tortor. Pellentesque non nisi vitae purus cursus tempor. Duis ipsum risus, hendrerit nec nulla eget, scelerisque convallis lacus. Donec quis commodo nibh, sit amet sollicitudin nisl. Interdum et malesuada fames ac ante ipsum primis in faucibus. Proin lobortis auctor diam, eget scelerisque diam ullamcorper non. Aenean et diam non lectus congue tempor. Sed ut fermentum ligula, non molestie nibh. Etiam vehicula libero in dolor egestas, in lacinia nisi placerat. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris auctor imperdiet nibh vel mollis. Ut sed libero aliquet, volutpat turpis et, aliquam est. Curabitur condimentum nunc ligula, id fringilla turpis facilisis eu. Nam cursus sit amet diam sit amet convallis.', '', 0),
(6, 'Berita 4', '2013-11-28', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ac vulputate odio. Donec fermentum augue laoreet mi mattis viverra. Morbi ac molestie tortor. Pellentesque non nisi vitae purus cursus tempor. Duis ipsum risus, hendrerit nec nulla eget, scelerisque convallis lacus. Donec quis commodo nibh, sit amet sollicitudin nisl. Interdum et malesuada fames ac ante ipsum primis in faucibus. Proin lobortis auctor diam, eget scelerisque diam ullamcorper non. Aenean et diam non lectus congue tempor. Sed ut fermentum ligula, non molestie nibh. Etiam vehicula libero in dolor egestas, in lacinia nisi placerat. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris auctor imperdiet nibh vel mollis. Ut sed libero aliquet, volutpat turpis et, aliquam est. Curabitur condimentum nunc ligula, id fringilla turpis facilisis eu. Nam cursus sit amet diam sit amet convallis.', '', 0),
(7, 'Berita 5', '2012-06-03', 'test', 'taiwan.png', 0);

-- --------------------------------------------------------

--
-- Table structure for table `traffic`
--

CREATE TABLE IF NOT EXISTS `traffic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) NOT NULL,
  `content` varchar(5000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `traffic`
--

INSERT INTO `traffic` (`id`, `judul`, `content`) VALUES
(1, 'Darana Test', 'Deskripsi Test');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `username` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
