<!DOCTYPE html>
<html lang="en">
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"
   />
   <link rel="stylesheet" type="text/css" href="aset/css/bootstrap.css">
   <link rel="stylesheet" type="text/css" href="aset/css/style.css" /> 
	<!--[if gt IE 8]>
	    <meta http-equiv="X-UA-Compatible" content="IE=edge" />        
	<![endif]--> 
    <title>Enjoy Jakarta</title>
    <style>
    .container {
      width: 920px;
      margin: 400px auto 0;
    }
    </style>
  </head>

	<body>
		<div class="container">
			<div class="span2 wellss2">
			    <a href="destination_list.php" class="btn btn-primary btn-large">
			        <i class="icon-pencil icon-white"></i>
			        <span><strong>Destination</strong></span>            
			    </a>
			</div>
			<div class="span2 wellss2">
			    <a href="event_list.php" class="btn btn-primary btn-large span1">
			        <i class="icon-eye-open icon-white"></i>
			        <span><strong>Events</strong></span>          
			    </a>
			</div>
			<div class="span2 wellss2">
			    <a href="news_list.php" class="btn btn-primary btn-large span1">
			        <i class="icon-edit icon-white"></i>
			        <span><strong>News</strong></span>       
			    </a>  
			</div>
			<div class="span2 wellss2">
			    <a href="trafic_list.php" class="btn btn-primary btn-large">
			        <i class="icon-edit icon-white"></i>
			        <span><strong>Traffic Info</strong></span>       
			    </a>  
			</div>
		</div>
	</body>

</html>