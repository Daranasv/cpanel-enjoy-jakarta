<?php
session_start();
include_once('include/connect.php');
if(isset($_POST['kirim'])){
  $nama = $_POST['nama'];
  $alamat = $_POST['alamat'];
  $telepon = $_POST['telepon'];
  $lat = $_POST['lat'];
  $long = $_POST['long'];
  $email = $_POST['email'];
  $link = $_POST['link'];
  $tipe = $_POST['tipe'];

  $url_photo=$_FILES['url_photo']['name'];

  $uploaddir='photo/';
  $alamatfile=$uploaddir.$url_photo;

  //periksa jika proses upload berjalan sukses
  $upload=move_uploaded_file($_FILES['url_photo']['tmp_name'],$alamatfile);

  $query =mysql_query("INSERT INTO  `destination` (  `nama` ,  `alamat` ,  `telepon` ,  `url_photo` ,  `lat` ,  `long` ,  `email` ,  `link` ,`tipe` )
  VALUES (
  '$nama',  '$alamat',  '$telepon',  '$url_photo',  '$lat',  '$long',  '$email',  '$link', '$tipe'
  )");
  if ($query) {
    echo "<div class='alert alert-success'>
              <button type='button' class='close' data-dismiss='alert'>×</button>
              <strong>Well done!</strong> You successfully input this data.
            </div>";
  } else {
    echo "<div class='alert alert-error'>
              <button type='button' class='close' data-dismiss='alert'>×</button>
              <strong>Oh snap!!</strong> Change a few things up and try submitting again.
            </div>";
  }
}

?>
<?php 
	if(isset($_GET['action']) && $_GET['action']=='edit'){
		$id 		= $_GET['id'];
		$resedit 	= mysql_query('SELECT * FROM destination where id='.$id);
		$rest 		= mysql_fetch_array($resedit);
		$nama 		= $rest['nama'];
		$alamat		= $rest['alamat'];
		$telepon	= $rest['telepon'];
		$lat 		= $rest['lat'];
		$long 		= $rest['long'];
		$tipe 		= $rest['tipe'];
		$email 		= $rest['email'];
		$link 		= $rest['link'];
		$tipe		= $rest['tipe'];
		$url 		= "edit_destination.php";
	}else{
		$id 		= "";
		$nama 		= "";
		$alamat		= "";
		$telepon	= "";
		$lat 		= "";
		$long 		= "";
		$tipe 		= "";
		$email 		= "";
		$link 		= "";
		$tipe		= "";
		$url 		= "";
	}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
   <link rel="stylesheet" type="text/css" href="aset/css/bootstrap.css">
   <link rel="stylesheet" type="text/css" href="aset/css/style.css" />  
  <!--[if gt IE 8]>
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />        
  <![endif]--> 
    <title>Enjoy Jakarta</title>
    <style>
    .wrappers, .alert {
      margin:70px auto;
    }
    </style>
  </head>
  <body>
  <div class="container">

    <div class="wrappers">
    <div class="wellss">
    <form action="<?php echo $url?>" method="POST" enctype="multipart/form-data">
    <legend>Destination</legend>
    <div class="row-fluid">
    <div class="span8">
        <label>Nama</label>
        <div class="input-prepend"><span class="add-on"><i class="icon-user"></i></span>
        <input type="text" id="" name="nama" value="<?php echo $nama?>"/>
        <input type='hidden' name='id' value='<?php echo $id?>'>
        </div>

        <label>Alamat</label>
        <textarea name="alamat" rows="3" style="min-width:235px;max-width:235px;"><?php echo $alamat?></textarea> 

        <label>Telepon</label>
        <div class="input-prepend"><span class="add-on"><i class="icon-th-large"></i></span>
        <input type="text" id="" name="telepon" value="<?php echo $telepon?>">
        </div>

        <label>Photo</label>
        <div class="input-prepend">
        <input type="file" id="" name="url_photo">
        </div>
    </div>
    <div class="span4" style="padding-left:30px;">
        <label>Latitude</label>
        <div class="input-prepend"><span class="add-on"><i class="icon-globe"></i></span>
        <input type="text" id="" name="lat" value="<?php echo $lat?>" />
        </div>

        <label>Longtitude</label>
        <div class="input-prepend"><span class="add-on"><i class="icon-globe"></i></span>
        <input type="text" id="" name="long" value="<?php echo $long?>">
        </div>      
        
        <label>Email</label>
        <div class="input-prepend"><span class="add-on"><i class="icon-envelope"></i></span>
        <input type="text" id="" name="email" value="<?php echo $email?>">
        </div>

        <label>Link</label>
        <div class="input-prepend"><span class="add-on"><i class="icon-envelope"></i></span>
        <input type="text" id="" name="link" value="<?php echo $link?>">
        </div>     
        <label>Type</label>
        <?php 
        	$arr_type = array('hotel'=>'Hotel',
        						'golf'=>'Golf',
        						'shopping'=>'Shopping',
        						'kuliner'=>'Kuliner',
        						'hiburan'=>'Hiburan',
        						'museum'=>'Museum',
        						'pusat rekeasi'=>'Pusat Rekreasi',
        						'spa'=>'Spa',
        						'mice'=>'Conventions, Exhibitions, and Events');
        ?>
        <select name="tipe" style="width:245px;">
        <?php if(isset($_GET['action'])&&$_GET['action']=='edit'):?>
        	<?php foreach ($arr_type as $key => $value):?>
        		<?php if($key == $tipe):?>
        			<option value='<?php echo $key?>' selected><?php echo $value?></option>
        		<?php else:?>
        			<option value='<?php echo $key?>'><?php echo $value?></option>
        		<?php endif;?>	
        	<?php endforeach;?>
        <?php else:?>
        	<?php foreach ($arr_type as $key => $value):?>
        		<option value='<?php echo $key?>'><?php echo $value?></option>
        	<?php endforeach;?>
        <?php endif;?>	
          
        </select>

        <a href='destination_list.php' class='btn btn-danger'>Kembali</a>
        <input type="submit" class="btn btn-success" value="Submit" name="kirim" />
    </div>
        </div>
      </form>
  </div>
  </div>

</div>
  </body>
</html>