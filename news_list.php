<?php include('include/connect.php');?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
	<title>Enjoy Jakarta</title>
	<!--<link rel="stylesheet" type="text/css" href="aset/css/bootstrap.css">-->
   	<link rel="stylesheet" type="text/css" href="aset/css/style.css" /> 
   	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
	<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
   	<style>
    .wrappers, .alert {
      margin:70px auto;
    }
    </style> 
</head>
<body>
	<div class='container'>
		<div class='row'>
			<div class='col-md-6 text-left'>
				<a href='menu.php' class='btn btn-default'>Kembali</a>
			</div>
			<div class='col-xs-6 text-right'>
				<a href='berita.php' class='btn btn-primary'>Tambah</a>
			</div>
		</div>
		<div class='row'>
			<div class="col-md-12 text-center">
			<?php 
			if(isset($_SESSION['alert'])){
				echo $_SESSION['alert'];
				//unset($_SESSION['alert']);
			}
			?>
				<h4>List Event</h4>
			</div>
		</div>
		<div class='row'>
			<div class='col-md-12'>
				<table class='table table-hover'>
					<thead>
						<tr>
							<th>ID</th>
							<th>Judul</th>
							<th>Tanggal</th>
							<th width="500px">Content</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php $res = mysql_query('select * from news');
							  while($data_table = mysql_fetch_array($res)):
						?>
						<tr>
							<td><?php echo $data_table['id']?></td>
							<td><?php echo  $data_table['judul']?></td>
							<td><?php echo  $data_table['tanggal']?></td>
							<td><?php echo $data_table['konten']?></td>
							<td>
								<a href='berita.php?action=edit&id=<?php echo $data_table["id"]?>' class='btn btn-warning btn-xs'>Edit</a>
								<a href='#' onclick='del(<?php echo $data_table['id']?>)' class='btn btn-danger btn-xs'>Delete</a>
							</td>
						</tr>
					<?php endwhile;?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<script>
		function del(id){
			if(confirm('Anda Yakin Akan Menghapus Data?')){
				window.location="delete_news.php?id="+id;
			}else{
				return false;
			}
		}
	</script>
</body>
</html>